<?php

	/*
		Sidebars for the products and tips pages.
	 
	 	@package Fruit Garden
	*/

if ( is_active_sidebar( 'products-sidebar' ) && is_singular( 'products' ) ) {
	dynamic_sidebar( 'products-sidebar' );
}
elseif ( is_active_sidebar( 'tips-sidebar' ) && is_singular( 'tips' ) ) {
	dynamic_sidebar( 'tips-sidebar' );
}
elseif ( is_page_template( 'template-home.php' ) ) {
	dynamic_sidebar( 'home-page-sidebar' );
}