<?php

	/*
		Template Name: Products Page
        
        @package Fruit Garden
	*/
		
get_header(); ?>
	
<!-- Prouducts title -->
<div class="fg-title-container">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="fg-title"><span><?php echo fg_option('products-title'); ?></span></h1>
			</div>
		</div>
	</div>
</div>
<!-- Products -->
<div class="products-container">
	<div class="container">
		<div class="row">
			<?php 
				$args = array( 
					'post_type' => 'products', 
					'posts_per_page'=> -1,
					'tax_query' => array( 
						array ( 
							'taxonomy'	=> 'product_type',
							'field'		=> 'slug',
							'terms'		=> 'sadnice'
						),
					),
				);
				$loop = new WP_Query( $args );
				
				if( $loop->have_posts() ):
					
					while( $loop->have_posts() ): $loop->the_post(); ?>
						
						<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">

							<article id="post-<?php the_ID(); ?>" class="products" >	

								<?php if ( has_post_thumbnail() ): ?>

									<div class="thumbnail"><?php the_post_thumbnail(); ?></div>
									<div class="product-title">
										<a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a>
									</div>
									<div class="excerpt"><?php the_excerpt('fruit_garden_excerpt_length'); ?></div>
									<div class="products-link-circle">
										<a href="<?php echo esc_url( get_permalink() ); ?>"><i class="fa fa-link"></i></a>
									</div>

								<?php else : ?>

									<div class="product-title">
										<a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a>
									</div>
									<div class="excerpt"><?php the_excerpt('fruit_garden_excerpt_length'); ?></div>
									<div class="products-link-circle">
										<a href="<?php echo esc_url( get_permalink() ); ?>"><i class="fa fa-link"></i></a>
									</div>

								<?php endif; ?>

							</article>

						</div>
					
					<?php endwhile;
					
		        endif;

		        wp_reset_postdata();
		    ?>
	    </div>
    </div>
</div>

<?php get_footer(); ?>