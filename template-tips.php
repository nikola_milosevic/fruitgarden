<?php

	/*
		Template Name: Tips Page
        
        @package Fruit Garden
	*/
		
get_header(); ?>

<!-- Tips title -->
<div class="fg-title-container">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="fg-title"><span><?php echo fg_option('tips-title'); ?></span></h1>
			</div>
		</div>
	</div>
</div>

<!-- Tips -->
<div class="tips-container">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 animated fadeInLeft">
				<div class="tip">
					<div class="box-1">
						<div class="col-xs-12">
							<div class="icon-1"></div>
						</div>
					</div>
					<h2><?php echo fg_option('tip-one-title'); ?></h2>
					<p><?php echo fg_option('tip-one-content'); ?></p>
					<a href="<?php echo esc_url( get_permalink(125) ); ?>" class="btn btn1" role="button"><?php esc_html_e( 'Detaljnije...', 'fruitgarden' ); ?></a>
				</div>
			</div> 
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 animated fadeInUp">
				<div class="tip">
					<div class="box-2">
						<div class="col-xs-12">
							<div class="icon-2"></div>
						</div>
					</div>
					<h2><?php echo $fg_option['tip-two-title']; ?></h2>
					<p><?php echo $fg_option['tip-two-content']; ?></p>
					<a href="<?php echo esc_url( get_permalink(121) ); ?>" class="btn btn2" role="button"><?php esc_html_e( 'Detaljnije...', 'fruitgarden' ); ?></a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 animated fadeInRight">
				<div class="tip">
					<div class="box-3">
						<div class="col-xs-12">
							<div class="icon-3"></div>
						</div>
					</div>
					<h2><?php echo $fg_option['tip-three-title']; ?></h2>
					<p><?php echo $fg_option['tip-three-content']; ?></p>
					<a href="<?php echo esc_url( get_permalink(119) ); ?>" class="btn btn3" role="button"><?php esc_html_e( 'Detaljnije...', 'fruitgarden' ); ?></a>
				</div>
			</div>
		</div><!-- .row -->
	</div><!-- .container -->
</div><!-- .tips-container -->
	
<?php get_footer(); ?>