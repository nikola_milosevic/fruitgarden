<?php

	/*
		The template used for displaying page content in single.php
	 
	 	@package Fruit Garden
	*/
	 	
?>

<article id="post-<?php the_id(); ?><?php post_class(); ?>">

	<div class="fg-title-container">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h1 class="fg-title"><span><?php the_title(); ?></span></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<div class="single-content">
					<?php edit_post_link(); ?>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>

</article>