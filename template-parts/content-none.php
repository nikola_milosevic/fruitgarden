<?php

	/*
		The main template file.

		This is the most generic template file in a WordPress theme
		and one of the two required files for a theme (the other being style.css).
		It is used to display a page when nothing more specific matches a query.
		E.g., it puts together the home page when no home.php file exists.
		Learn more: http://codex.wordpress.org/Template_Hierarchy
	 
	 	@package Fruit Garden
	*/

?>

<article id="post-<?php the_id(); ?><?php post_class(); ?> none-container">

	<div class="container-fluid">
		<div class="row">
			<header class="entry-header text-center">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->
			<div class="entry-content text-center">
				<?php the_content(); ?>
			</div><!-- .entry-content -->
			<footer class="entry-footer text-center">
				<?php edit_post_link( __( 'Edit', 'fruitgarden' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-footer -->
		</div><!-- .row -->
	</div><!-- .container-fluid -->

</article>