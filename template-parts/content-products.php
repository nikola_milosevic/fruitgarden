<?php

	/*
		The template used for displaying content on the products page.
	 
	 	@package Fruit Garden
	*/
	 	
?>

<article id="post-<?php the_id(); ?>">
	<div class="row">
		<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-1 col-lg-8 product-content">
			<h1 class="single-title"><span><?php the_title(); ?></span></h1>
			<?php edit_post_link(); ?>
			<?php the_content(); ?>
		</div><!-- .product-content -->
		<div class="col-xs-8 col-sm-3 col-md-2 col-lg-2 products-sidebar">
			<?php $sidebarlogo = esc_attr( get_option('site_logo') ); ?>
			<div class="sidebar-logo" style="background-image: url(<?php print $sidebarlogo; ?>);">
			</div><!-- .sidebar-logo -->
			<?php get_sidebar( 'product-sidebar' ); ?>
		</div><!-- .product-sidebar -->
	</div>
</article>
	
