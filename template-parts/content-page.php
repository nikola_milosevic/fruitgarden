<?php

	/*
		The template used for displaying page content in page.php
	 
	 	@package Fruit Garden
	*/
	 	
?>

<article id="post-<?php the_id(); ?><?php post_class(); ?>">

	<div class="fg-title-container">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h1 class="fg-title"><span><?php the_title(); ?></span></h1>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="entry-content text-center">
					<?php esc_html_e( 'Select a page template.', 'fruitgarden' ); ?>
				</div>
			</div>
			<div class="col-xs-12 text-center">
				<?php edit_post_link( __( 'Select', 'fruitgarden' ), '<span class="btn btn-admin" role="button">', '</span>' ); ?>
			</div>
		</div>
	</div>
	

</article>