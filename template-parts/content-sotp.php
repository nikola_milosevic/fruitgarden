<?php

	/*
		The template used for displaying some of the products on the home page.
	 
	 	@package Fruit Garden
	*/
	 	
?>
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
	<div class="product-items">
		<?php if ( has_post_thumbnail() ): ?>
			<?php the_post_thumbnail(); ?>
		<?php endif; ?>
		<div class="mask">
			<h2><?php the_title(); ?></h2>
			<?php the_excerpt('custom_excerpt_length'); ?>
			<a href="<?php echo esc_url( get_permalink() ); ?>" class="info">Detaljinije...</a>
		</div>
	</div>
</div>