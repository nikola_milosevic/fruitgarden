<?php

	/*
		The header file.

		Displays all of the <head> section and everything up till <div id="content">

		@package Fruit Garden
	*/

?>
<!DOCTYPE html>

<html <?php language_attributes(); ?>>

	<head>
		<title><?php bloginfo('name'); wp_title( '/', true, 'left' ); ?></title>
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php if( is_singular() && pings_open( get_queried_object() ) ): ?>
			<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php endif; ?>
		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/site-logo40x30.png">
		<?php wp_head(); ?>
	</head>

	<body>

		<?php if ( is_home() || is_front_page() || is_page() ) : ?>
		<!-- Header image -->
		<!-- <?php $background_image = fg_option( 'header-image', false, 'url' ); ?>
		<header class="header-container" style="background-image: url(<?php echo esc_url( $background_image ); ?>);">
			<div class="container-fluid">
				<div class="row">
					<?php $logo = fg_option( 'header-logo', false, 'url' ); ?>
					<img class="logo" src="<?php echo esc_url( $logo ); ?>">
				</div>
			</div>
		</header> -->

		<!-- Nav menu -->
		<!-- <nav class="navbar fg-navbar" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#fg-navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
			    </div>

		        <?php
		            wp_nav_menu( array(
		                'menu'              => 'primary',
		                'theme_location'    => 'primary_menu',
		                'depth'             => 2,
		                'container'         => 'div',
		                'container_class'   => 'collapse navbar-collapse',
		        		'container_id'      => 'fg-navbar-collapse',
		                'menu_class'        => 'nav navbar-nav',
		                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		                'walker'            => new wp_bootstrap_navwalker()
		                )
		            );
		        ?>
			</div>
		</nav> -->

		<!-- Nav menu -->
		<nav class="fg-navbar fg-content-navbar" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#fg-navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<img class="navbar-brand" src="<?php echo get_template_directory_uri(); ?>/img/site-logo.png">
			    </div>

		        <?php
		            wp_nav_menu( array(
		                'menu'              => 'primary',
		                'theme_location'    => 'primary_menu',
		                'depth'             => 2,
		                'container'         => 'div',
		                'container_class'   => 'collapse navbar-collapse',
		        		'container_id'      => 'fg-navbar-collapse',
		                'menu_class'        => 'nav navbar-nav',
		                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		                'walker'            => new wp_bootstrap_navwalker()
		                )
		            );
		        ?>
			</div>
		</nav>

		<?php else : ?>
			
		<nav class="fg-navbar fg-content-navbar" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#fg-navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<img class="navbar-brand" src="<?php echo get_template_directory_uri(); ?>/img/site-logo.png">
			    </div>

		        <?php
		            wp_nav_menu( array(
		                'menu'              => 'primary',
		                'theme_location'    => 'primary_menu',
		                'depth'             => 2,
		                'container'         => 'div',
		                'container_class'   => 'collapse navbar-collapse',
		        		'container_id'      => 'fg-navbar-collapse',
		                'menu_class'        => 'nav navbar-nav',
		                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		                'walker'            => new wp_bootstrap_navwalker()
		                )
		            );
		        ?>
			</div>
		</nav>
		<?php endif; ?>