<?php 

    /*
        Template Name: Contact Page
        
        @package Fruit Garden
    */

get_header(); ?>

<!-- Contact title -->
<div class="fg-title-container">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="fg-title"><span><?php echo fg_option('contact-title'); ?></span></h1>
			</div>
		</div>
	</div>
</div>

<!-- Contact -->
<div class="contact-container">
	<div class="container">
		<div class="row">
			<div class="fg-form-body col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<form id="contact-form" method="post" class="form-horizontal" autocomplete="off" action="<?php echo get_template_directory_uri(). '/inc/contact.php'; ?>">
					
					<div class="col-sm-12">
						<label class="control-label" for="name">Ime:</label>
						<input type="text" id="name" name="name" placeholder="Vaše ime" />
					</div>
					<div class="col-sm-12">
						<label class="control-label" for="subject">Naslov poruke:</label>
						<input type="text" id="subject" name="subject" placeholder="Naslov vaše poruke" />
					</div>
					<div class="col-sm-12">
						<label class="control-label" for="email">Email:</label>
						<input type="text" id="email" name="email" placeholder="Vaš email" />
					</div>
					<div class="col-sm-12">
						<label class="control-label" for="message">Poruka:</label>
						<textarea id="message" name="message" placeholder="Vaša poruka" /></textarea>
					</div>
					<div class="col-sm-12 pull-right">
						<button id="send-mail" type="submit" class="btn" value="Send">Pošalji</button>
						<div id="success" style="display: none;">Poruka poslata!</div>
						<div id="error" style="display: none;">Molimo popunite sva polja.</div>
					</div>

				</form><!-- .contact-form -->
			</div><!-- .fg-form-body -->
			<div class="contact-information col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="col-xs-12">
					<h3><?php echo fg_option('map-title'); ?></h3>
					<div id="googleMap"></div>
				</div>
				<div class="col-xs-6">
					<h3><?php echo fg_option('addres-title'); ?></h3>
					<p><i class="fa fa-map-marker"></i><?php echo fg_option('fg-addres'); ?></p>
					<p><i class="fa fa-phone"></i><?php echo fg_option('fg-phone'); ?></p>
				</div>
			</div><!-- .map -->
		</div><!-- .row -->
	</div><!-- .container -->
</div><!-- .contact-container -->

<?php get_footer(); ?>