<?php

	/*

		The template for displaying 404 pages (not found).

		@package Fruit Garden

	*/
		
get_header(); ?>

	<div class="container-fluid">
		<div class="row">
			<div id="primary">
				<main id="main" class="site-main" role="main">
					<div class="error-404 not-found text-center">
						<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'fruitgarden' ); ?></h1>
						<p><?php _e( 'It looks like nothing was found at this location.', 'fruitgarden' ); ?></p>
					</div>
				</main>
			</div>
		</div>
	</div>

<?php get_footer(); ?>