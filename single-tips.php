<?php

	/*
		The single tip file.

		This is the single tip file, it is used to display a single tip page.
	 
	 	@package Fruit Garden
	*/

get_header(); ?>

<div class="single-container">
	<div class="container">
		<div class="row">
			<?php if( have_posts() ):
				while( have_posts() ): the_post(); ?>

				<article id="post-<?php the_id(); ?>">
					<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-1 col-lg-8 product-content">
						<h1 class="single-title"><span><?php the_title(); ?></span></h1> 
						<p><?php edit_post_link(); ?></p>
						<p><?php the_content(); ?></p>
					</div><!-- .product-content -->
					<div class="col-xs-8 col-sm-3 col-md-2 col-lg-2 products-sidebar">
						<?php $sidebarlogo = esc_attr( get_option('site_logo') ); ?>
						<div class="sidebar-logo" style="background-image: url(<?php print $sidebarlogo; ?>);">
						</div><!-- .sidebar-logo -->
						<?php get_sidebar( 'product-sidebar' ); ?>
					</div><!-- .product-sidebar -->
					<button class="gototop"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
				</article>

				<?php endwhile;
			endif; ?>
		</div><!-- .row -->
	</div><!-- .container-fluid -->
</div><!-- .single-container -->

<?php get_footer(); ?>