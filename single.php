<?php

	/*
		The template for displaying all singe blog files.

		This is the template that displays all pages by default.
		Please note that this is the WordPress construct of pages
		and that other 'pages' on your WordPress site will use a
		different template.

		@package Fruit Garden
	*/


get_header(); ?>

<div class="single-container">
	<div class="container-fluid">
		<div class="row">
			<?php if( have_posts() ):
				while( have_posts() ): the_post(); ?>

				<?php get_template_part( 'template-parts/content', get_post_type() ); ?>

				<?php endwhile;
			endif; ?>
		</div><!-- .row -->
	</div><!-- .container-fluid -->
</div><!-- .single-container -->

<?php get_footer(); ?>