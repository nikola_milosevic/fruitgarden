<?php

	/*
		This is the template for the footer

		@package Fruit Garden

	*/

?>

		<footer class="fruitgarden-footer">
			<div class="container">
			<!-- Komentar -->
				<div class="col-md-6 col-sm-6 col-xs-12 footer-links">
					<?php dynamic_sidebar( 'footer-links' ); ?> 
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12 footer-contact-info">
					<?php dynamic_sidebar( 'footer-contact-info' ); ?> 
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12 footer-logo">
					<?php $sitelogo = esc_attr( get_option('site_logo') ); ?>
					<div class="footer-img" style="background-image: url(<?php print $sitelogo; ?>);"></div>
				</div>
				<div class="col-xs-12 container-copyright-icons">
					<div class="col-sm-8 col-xs-12">
						<p class="footer-copyright">Copyright &copy; <?php echo date("Y");?> <?php echo fg_option('footer-copyright-text'); ?> Developed by <a href="http://nikolamilosevicdev.com/" target="_blank">Nikola Milošević</a></p>
					</div>
					<div class="col-sm-4 col-xs-12">
						<p class="footer-icons">
							<?php global $fg_option; ?>
							<?php $social_options = $fg_option['social-icons']; ?>
							<?php foreach ( $social_options as $key => $value ) {
								if ( $value && $key == 'Facebook' ) { ?>
									<a href="<?php echo $value; ?>" target="_blank"><i class="fa fa-facebook"></i></a>
								<?php } elseif ( $value && $key == 'Twitter' ) { ?>
									<a href="<?php echo $value; ?>" target="_blank"><i class="fa fa-twitter"></i></a>
								<?php } elseif ( $value && $key == 'LinkedIn' ) { ?>
									<a href="<?php echo $value; ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
								<?php }
							} ?>
						</p>
					</div>
				</div>
			</div>
		</footer>
	<?php wp_footer(); ?>
	</body>
</html>