<?php 

    /*
        Template Name: Gallery Page
        
        @package Fruit Garden
    */

get_header(); ?>

<!-- Gallery title -->
<div class="fg-title-container">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="fg-title"><span><?php the_title(); ?></span></h1>
			</div>
		</div>
	</div>
</div>

<!-- Gallery -->
<div class="gallery-container">
	<div class="container">
		<div class="row">
			<div class="fg-gallery">
				<?php 
		            $args = array( 'post_type' => 'fg-gallery' );

		            $loop = new WP_Query( $args );
		                
		                if( $loop->have_posts() ):
		                    
		                    while( $loop->have_posts() ): $loop->the_post(); ?>
		                        
		                        <?php fruit_garden_output_gallery( 'fruitgarden_gallery_field' ); ?>
		                    
		                    <?php endwhile; 
		                    
		                endif;

		            wp_reset_postdata();
            	?>
			</div>
		</div><!-- .row -->
	</div><!-- .container -->
</div><!-- .gallery-container -->

<?php get_footer(); ?>