<?php 

    /*
        Template Name: Home Page
        
        @package Fruit Garden
    */

get_header(); ?>

<!-- Slideshow -->
<div class="slider-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 slider"> 
                <div class="flexslider">
                    <ul class="slides">
                        <?php if (isset($fg_option['slider']) && !empty($fg_option['slider'])) : ?>
                            <?php foreach( $fg_option['slider'] as $slide ) : ?>
                                <li data-thumb="<?php echo esc_url( $slide['image'] ); ?>">
                                    <img src="<?php echo esc_url( $slide['image'] ); ?>">
                                    <div class="flex-caption">
                                        <?php echo esc_attr( $slide['description'] ); ?>
                                    </div>
                                </li>
                            <?php endforeach; ?>  
                        <?php endif; ?>
                    </ul><!-- .slides -->
                </div><!-- .flexslider -->
            </div><!-- .col-sm-10 -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .slider-container -->

<div class="home-container">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
                <div class="home-content">
                    <!-- Welcome text -->
                    <div class="welcome-text">
                        <h1 class="homepage-title"><?php echo esc_html( fg_option('welcome-title') ); ?></h1>
                        <p><?php echo esc_html( fg_option('welcome-text') ); ?></p>
                    </div>
                    <!-- Some of the products -->
                    <div class="sotp-items">
                        <h1 class="homepage-title"><?php echo esc_html( fg_option('sotp-title') );  ?></h1>
                        <div class="row">
                            <?php 
                                $args = array( 
                                    'post_type' => 'products', 
                                    'posts_per_page'   => 3,
                                    'tax_query' => array( 
                                        array( 
                                            'taxonomy'  => 'product_type',
                                            'field'     => 'slug',
                                            'terms'     => 'sadnice'
                                        ),
                                    ),
                                );
                                $loop = new WP_Query( $args );
                                    
                                    if( $loop->have_posts() ):
                                        
                                        while( $loop->have_posts() ): $loop->the_post(); ?>
                                            
                                            <?php get_template_part('template-parts/content', 'sotp'); ?>
                                        
                                        <?php endwhile;
                                        
                                    endif;

                                wp_reset_postdata();
                            ?>
                        </div>
                    </div>
                    <!-- Tips -->
                    <div class="interesting">
                        <h1 class="homepage-title"><?php echo esc_html( fg_option('interesting-title') ); ?></h1>
                        <div class="interesting-list js-">
                            <div role="tabpanel">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <?php 
                                        $count_active = 0;
                                        $count_id = 0;
                                        $bullets = ''; 
                                    ?>
                                    <?php if ( fg_option( 'fg-interesting' ) != '') : ?>
                                    <?php foreach ( fg_option('fg-interesting') as $text) : ?>
                                    <div role="tabpanel" class="tab-pane animated bounceIn <?php if($count_active == 0): echo 'active'; endif; ?>" id="<?php echo $count_id; ?>">

                                        <div class="interesting-text">
                                            <p><?php echo esc_html( $text['description'] ); ?></p>
                                        </div>

                                    </div>
                                    <?php 
                                    $bullets .= 
                                        '<li role="presentation" class="'; ?>
                                        <?php if($count_active == 0): $bullets .='active'; endif; ?>
                                        <?php  $bullets .= '">
                                            <a href="#'.$count_id.'" aria-controls="'.$count_id.'" role="tab" data-toggle="tab"></a>
                                        </li>'; 
                                    ?>
                                    <?php 
                                        $count_id++;
                                        $count_active++;
                                        endforeach;
                                        endif;
                                    ?>
                                    <!-- Nav tabs --> 
                                    <ul class="nav nav-tabs" role="tablist">
                                        <?php echo $bullets; ?>
                                    </ul>
                                </div><!-- .tab-content -->
                            </div><!-- .tabpanel --> 
                        </div><!-- .interesting-list -->
                    </div><!-- .interesting -->
                </div><!-- .home-content -->
            </div><!-- .col-xs-9 -->
            <div class="col-xs-8 col-sm-3 col-md-2 col-lg-2">
                <div class="home-sidebar-container">
                    <?php get_sidebar( 'product-sidebar' ); ?>
                </div>
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .home-container -->

<?php get_footer(); ?>