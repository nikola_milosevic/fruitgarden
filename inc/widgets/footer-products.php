<?php

	/*
		Plugin Name: Recent Products
		Plugin URI: recent-products
		Description: Widget to display recent products
		Version: 1.0
		Author: nikolahisar
		Author URI: http://www.google.rs
	*/
		
class Recent_Products extends WP_Widget {

	//Register widget with WordPress.
	function __construct() {
		parent::__construct(
			'list_of_products', // Base ID
			__( 'List of Products', 'fruitgarden' ), // Name
			array( 'description' => __( 'Display List of Products in the Footer', 'fruitgarden' ), ) // Args
		);
	}
	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		$query = array( 
            'post_type' => 'products', 
            'posts_per_page'   => -1,
            'tax_query' => array( 
                array( 
                    'taxonomy'  => 'product_type',
                    'field'     => 'slug',
                    'terms'     => 'sadnice'
                ),
            ),
        );
		?>
		<ul class="col-sm-9 col-xs-12">
			<?php $product = new WP_Query($query); while($product->have_posts()): $product->the_post(); ?>
				<li>
					<a class="link" href="<?php echo get_permalink(); ?>" data-hover="<?php the_title(); ?>"><?php the_title(); ?></a>
				</li>
			<?php endwhile; wp_reset_postdata(); ?>
		</ul>
		<?php echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'text_domain' );
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( esc_attr( 'Title:' ) ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

}