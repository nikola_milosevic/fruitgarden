<?php

	/*
		Plugin Name: Footer Contact
		Plugin URI: footer-contact
		Description: Widget to display footer contact informations.
		Version: 1.0
		Author: nikolahisar
		Author URI: http://www.google.rs
	*/
		
class Footer_Contact_Form extends WP_Widget {

	//Register widget with WordPress.
	function __construct() {
		parent::__construct(
			'footer_contact_widget', // Base ID
			__( 'Footer Contact Form', 'fruitgarden' ), // Name
			array( 'description' => __( 'Display Display Contact Form', 'fruitgarden' ), ) // Args
		);
	}
	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		extract( $args );

	    // variables from the widget settings
   		$title = apply_filters('widget_title', $instance['title'] );
	    $addres = $instance['addres'];
	    $phone = $instance['phone'];
	    $skype = $instance['skype'];
	    $email = $instance['email'];

	    // Before widget (defined by theme functions file)
	    echo $before_widget;

	    // Display widget title
	    if ( $title )
	        echo $before_title . $title . $after_title;

	    // Display the author addres
	    if ( $addres )
	        echo '<div class="footer-contact-text"><p><i class="fa fa-map-marker"></i><span>Adresa:</span>'.$addres.'</p>';
	    
	    // Display author phone
	    if ( $phone )
	        echo '<p><i class="fa fa-phone"></i><span>Telefon:</span>'.$phone.'</p>';

	    // Display author skype
	    if ( $skype )
	        echo '<p><i class="fa fa-user"></i><span>Skype:</span>'.$skype.'</p>';
	        
	    // Display author email
	    if ( $email )
	        echo '<p><i class="fa fa-envelope"></i><span>Email:</span><a href="mailto:'.$email.'">'.$email.'</a></p></div>';

	    // After widget (defined by theme functions file)
	    echo $after_widget;
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		 // Set up some default widget settings
    $defaults = array(

        'title' => '',
        'addres' => '',
        'phone' => '',
        'skype' => '',
        'email' => '',
    );
        
    $instance = wp_parse_args( (array) $instance, $defaults ); ?>

    <!-- Widget Title: Text Input -->
    <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:') ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
    </p>
    
    <!-- Name: Text Input -->
    <p>
        <label for="<?php echo $this->get_field_id( 'addres' ); ?>"><?php _e('Addres:') ?></label>
        <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'addres' ); ?>" name="<?php echo $this->get_field_name( 'addres' ); ?>" value="<?php echo $instance['addres']; ?>" />
    </p>
    
    <!-- Introduction: Text Input -->
    <p>
        <label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e('Phone:') ?></label>
        <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" value="<?php echo $instance['phone']; ?>" />
    </p>
    
    <!-- Location: Text Input -->
    <p>
        <label for="<?php echo $this->get_field_id( 'skype' ); ?>"><?php _e('Skype:') ?></label>
        <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'skype' ); ?>" name="<?php echo $this->get_field_name( 'skype' ); ?>" value="<?php echo $instance['skype']; ?>" />
    </p>
    
    <!-- Email: Text Input -->
    <p>
        <label for="<?php echo $this->get_field_id( 'email' ); ?>"><?php _e('Email:') ?></label>
        <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'email' ); ?>" name="<?php echo $this->get_field_name( 'email' ); ?>" value="<?php echo $instance['email']; ?>" />
    </p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	/**
	 * Update Widget
	 */
	function update( $new_instance, $old_instance ) {
	    $instance = $old_instance;
	    
	    // Strip tags to remove HTML (important for text inputs)
	    $instance = array();
	    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	    
	    // No need to strip tags
	    $instance['name'] = $new_instance['name'];
	    $instance['addres'] = $new_instance['addres'];
	    $instance['phone'] = $new_instance['phone'];
	    $instance['skype'] = $new_instance['skype'];
	    $instance['email'] = $new_instance['email'];
	    
	    return $instance;
	}

}