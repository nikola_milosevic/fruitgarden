<?php

	/*
		Plugin Name: Sorte
		Plugin URI: sorte
		Description: Widget za prikazivanje sorti u sidebaru
		Version: 1.0
		Author: Nikola Milošević
		Author URI: http://www.nikolamilosevicdev.com
	*/
		
class Sorte extends WP_Widget {

	//Register widget with WordPress.
	function __construct() {
		parent::__construct(
			'sorte_sidebar_widget', // Base ID
			__( 'Sorte', 'fruitgarden' ), // Name
			array( 'description' => __( ' Widget za prikazivanje sorti u sidebaru', 'fruitgarden' ), ) // Args
		);
	}
	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		?>
		<?php 
		if ( is_single( '69' ) ){
			$slug = 'tresnje';
		}elseif( is_single( '32' ) ){
			$slug = 'jabuke';
		}elseif( is_single( '57' ) ){
			$slug = 'kruske';
		}elseif( is_single( '65' ) ){
			$slug = 'sljive';
		}elseif( is_single( '77' ) ){
			$slug = 'breskva';
		}elseif( is_single( '79' ) ){
			$slug = 'patuljasto_voce';
		}elseif( is_single( '81' ) ){
			$slug = 'visnja';
		}elseif( is_single( '89' ) ){
			$slug = 'dunje';
		}elseif( is_single( '91' ) ){
			$slug = 'kajsija';
		}elseif( is_single( '93' ) ){
			$slug = 'musmula';
		}elseif( is_single( '95' ) ){
			$slug = 'aronija';
		}elseif( has_term( 'tresnje', 'product_type' ) ){
			$slug = 'tresnje';
		}elseif( has_term( 'jabuke', 'product_type' ) ){
			$slug = 'jabuke';
		}elseif( has_term( 'kruske', 'product_type' ) ){
			$slug = 'kruske';
		}elseif( has_term( 'sljive', 'product_type' ) ){
			$slug = 'sljive';
		}elseif( has_term( 'breskva', 'product_type' ) ){
			$slug = 'breskva';
		}elseif( has_term( 'patuljasto_voce', 'product_type' ) ){
			$slug = 'patuljasto_voce';
		}elseif( has_term( 'visnja', 'product_type' ) ){
			$slug = 'visnja';
		}elseif( has_term( 'dunje', 'product_type' ) ){
			$slug = 'dunje';
		}elseif( has_term( 'kajsija', 'product_type' ) ){
			$slug = 'kajsija';
		}elseif( has_term( 'musmula', 'product_type' ) ){
			$slug = 'musmula';
		}elseif( has_term( 'aronija', 'product_type' ) ){
			$slug = 'aronija';
		}
		?>
		<?php
		$query = array(
			'post_type' => 'products', 
			'posts_per_page'=> -1,
			'tax_query' => array( 
				array( 
					'taxonomy'	=> 'product_type',
					'field'		=> 'slug',
					'terms'		=> $slug
				),
			),
			'orderby'			=> 'date',
			'order'				=> 'ASC'
		);
		?>
		<?php $product = new WP_Query($query); while($product->have_posts()): $product->the_post(); ?>
		<ul>
			<li>
				<a href="<?php echo get_permalink(); ?>" data-hover="<?php the_title(); ?>"><?php the_title(); ?></a>
			</li>
		</ul>
		<?php endwhile; wp_reset_postdata(); ?>
		
		<?php echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'text_domain' );
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( esc_attr( 'Title:' ) ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title );?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

}