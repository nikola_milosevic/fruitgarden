<?php


define( 'WP_USE_THEMES', false );
require_once("../../../../wp-load.php");

//your email address 
$to = fg_option('contact-email');

$name = sanitize_text_field( $_POST["name"] );
$subject = sanitize_text_field( $_POST["subject"] );
$email = sanitize_email( $_POST["email"] );
$message = sanitize_text_field( $_POST["message"] );
$headers  = 'From: ' . $name . ' <' . $email . '>' . "\r\n";
$headers .= "Reply-To: $email\r\n";

// Send the email using wp_mail()
if ( empty($name && $email && $subject && $message ) ) {
	echo "fail";
} else {
	echo "success";
	wp_mail( $to, $subject, $message, $headers );
}