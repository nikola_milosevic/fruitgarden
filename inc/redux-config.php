<?php
    /**
     * ReduxFramework Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "fg_option";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

    /*
    
        ---> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
    
    */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();
    
    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        } 
    }

    /*
        ---> SET ARGUMENTS
        All the possible arguments for Redux.
        For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
    */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Theme Options', 'fruitgarden' ),
        'page_title'           => __( 'Theme Options', 'fruitgarden' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => false,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   
        // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

    );

    Redux::setArgs( $opt_name, $args );

    /*
        <--- END ARGUMENTS
    */

    /*
    
        ---> START SECTIONS
    
    */

    /*
    
        ---> Header
    
    */
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Header', 'fruitgarden' ),
        'id'               => 'header',
        'customizer_width' => '400px',
        'icon'             => 'el el-website',
        'fields'           => array(
            array(
                'id'       => 'header-image',
                'type'     => 'media',
                'title'    => __( 'Header background image', 'fruitgarden' ),
                'desc'     => __( 'This is a header background image.', 'fruitgarden' ),
                'subtitle' => __( 'Upload site background image.', 'fruitgarden' ),
            ),
            array(
                'id'       => 'header-logo',
                'type'     => 'media',
                'title'    => __( 'Header logo', 'fruitgarden' ),
                'desc'     => __( 'This is a site logo.', 'fruitgarden' ),
                'subtitle' => __( 'Upload site logo.', 'fruitgarden' ),
            ),
        )
    ) );

    /*
    
        ---> Home Page
    
    */
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Home', 'fruitgarden' ),
        'id'               => 'home',
        'desc'             => __( 'These are really basic fields!', 'fruitgarden' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-home'
    ) );
    // Slider
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Slideshow', 'fruitgarden' ),
        'id'         => 'home-slider',
        'desc'       => __( 'Upload your images for slideshow. Note: "recomended image size 960x540px" ', 'fruitgarden' ),
        'subsection' => true,
        'fields'     => array(
            array(
                'id'          => 'slider',
                'type'        => 'slides',
                'title'       => __( 'Slides Options', 'fruitgarden' ),
                'subtitle'    => __( 'Unlimited slides with drag and drop sortings.', 'fruitgarden' ),
                'desc'        => __( 'This field will store all slides values into a multidimensional array to use into a foreach loop.', 'fruitgarden' ),
                'placeholder' => array(
                    'title'       => __( 'This is a title', 'fruitgarden' ),
                    'description' => __( 'Description Here', 'fruitgarden' ),
                    'url'         => __( 'Give us a link!', 'fruitgarden' ),
                ),
            ),
        )
    ) );
    // Text Area
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Welcome text', 'fruitgarden' ),
        'id'         => 'home-textarea',             
        'fields'     => array(
            array(
                'id'       => 'welcome-title',
                'type'     => 'text',
                'title'    => __( 'Title', 'fruitgarden' ),
                'subtitle' => __( 'Welcome section',  'fruitgarden' ),
                'desc'     => __( 'Enter your title in welcome section below slider.', 'fruitgarden' ),
                'default'  => 'Welcome to our awesome site',
            ),
            array(
                'id'       => 'welcome-text',
                'type'     => 'textarea',
                'title'    => __( 'Text area', 'fruitgarden' ),
                'subtitle' => __( 'Welcome section', 'fruitgarden' ),
                'desc'     => __( 'Your text here.', 'fruitgarden' ),
                'default'  => 'Default Text',
            ),
        ),
        'subsection' => true
    ) );
    // Section for some of the products
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Some of the Products' ,'fruitgarden' ),
        'id'        => 'some-of-the-prodicts',
        'fields'    => array(
            array(
                'id'        => 'sotp-title',
                'type'      => 'text',
                'title'     => __( 'Title' ),
                'subtitle'  => __( 'Some of the products title', 'fruitgarden' ),
                'desc'      => __( 'Enter your title in some of the products section.', 'fruitgarden' ),
                'default'    => 'Some of the Products',
            ),
        ),
        'subsection' => true
    ));
    // Tab Panel
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Interesting', 'fruitgarden' ),
        'id'         => 'fg-tab-panel',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'        => 'interesting-title',
                'type'      => 'text',
                'title'     => __( 'Title' ),
                'subtitle'  => __( 'Interesting title', 'fruitgarden' ),
                'desc'      => __( 'Enter your title in the interesting section.', 'fruitgarden' ),
                'default'    => 'Interesting',
            ),
            array(
                'id'          => 'fg-interesting',
                'type'        => 'slides',
                'title'       => __( 'Skill Options', 'fruitgarden' ),
                'subtitle'    => __( 'Unlimited Skill Box with drag and drop sortings.', 'fruitgarden' ),
                'desc'        => __( 'Create skill boxes for skill section.', 'fruitgarden' ),
                'show'        => array( 
                    'title'         => true,
                    'description'   => true,
                    'url'           => false,
                    'image_upload'  => false,
                ),
                'placeholder' => array(
                    'title'           => __( 'Skill tilte here', 'fruitgarden' ),
                    'subtitle'        => __( 'Skill number here. Max: 100%.', 'fruitgarden' ),

                ),
            ),
        ),
    ) );
    /*
    
        ---> Products Page
    
    */
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Products', 'fruitgarden' ),
        'id'               => 'products-page',
        'desc'             => __( 'Product fields', 'fruitgarden' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-th'
    ) );
    // Text Area
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Products page', 'fruitgarden' ),
        'id'         => 'products-textarea',             
        'fields'     => array(
            array(
                'id'       => 'products-title',
                'type'     => 'text',
                'title'    => __( 'Title', 'fruitgarden' ),
                'subtitle' => __( 'Products page',  'fruitgarden' ),
                'desc'     => __( 'Enter your title in products page!', 'fruitgarden' ),
                'default'  => 'List of Products',
            ),
        ),
        'subsection' => true
    ) );
    /*
    
        ---> Tips Page
    
    */
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Tips', 'fruitgarden' ),
        'id'               => 'tips-page',
        'desc'             => __( 'Tips fields', 'fruitgarden' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-edit'
    ) );
    // Text Area
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Tips page', 'fruitgarden' ),
        'id'         => 'tips-textarea',             
        'fields'     => array(
            array(
                'id'       => 'tips-title',
                'type'     => 'text',
                'title'    => __( 'Tips Page Title', 'fruitgarden' ),
                'subtitle' => __( 'Tips page',  'fruitgarden' ),
                'desc'     => __( 'Enter your title in tips page!', 'fruitgarden' ),
                'default'  => 'Tips',
            ),
            array(
                'id'       => 'tip-one-title',
                'type'     => 'text',
                'title'    => __( 'Tip first box title', 'fruitgarden' ),
                'subtitle' => __( 'Tips page',  'fruitgarden' ),
                'desc'     => __( 'Enter your title in tip box!', 'fruitgarden' ),
                'default'  => 'Tips Sample',
            ),
            array(
                'id'       => 'tip-one-content',
                'type'     => 'textarea',
                'title'    => __( 'Tip first box content', 'fruitgarden' ),
                'subtitle' => __( 'Tips page',  'fruitgarden' ),
                'desc'     => __( 'Enter your content in the first box!', 'fruitgarden' ),
                'default'  => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            ),
            array(
                'id'       => 'tip-two-title',
                'type'     => 'text',
                'title'    => __( 'Tip first box title', 'fruitgarden' ),
                'subtitle' => __( 'Tips page',  'fruitgarden' ),
                'desc'     => __( 'Enter your title in tip box!', 'fruitgarden' ),
                'default'  => 'Tips Sample',
            ),
            array(
                'id'       => 'tip-two-content',
                'type'     => 'textarea',
                'title'    => __( 'Tip second box content', 'fruitgarden' ),
                'subtitle' => __( 'Tips page',  'fruitgarden' ),
                'desc'     => __( 'Enter your content in the second box!', 'fruitgarden' ),
                'default'  => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            ),
            array(
                'id'       => 'tip-three-title',
                'type'     => 'text',
                'title'    => __( 'Tip first box title', 'fruitgarden' ),
                'subtitle' => __( 'Tips page',  'fruitgarden' ),
                'desc'     => __( 'Enter your title in tip box!', 'fruitgarden' ),
                'default'  => 'Tips Sample',
            ),
            array(
                'id'       => 'tip-three-content',
                'type'     => 'textarea',
                'title'    => __( 'Tip third box content', 'fruitgarden' ),
                'subtitle' => __( 'Tips page',  'fruitgarden' ),
                'desc'     => __( 'Enter your content in the third box!', 'fruitgarden' ),
                'default'  => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            ),
        ),
        'subsection' => true
    ) );
    $social_options = array(
        'Facebook'    => 'Facebook',
        'Twitter'     => 'Twitter',
        'LinkedIn'    => 'LinkedIn',
    );
    /*
    
        ---> Contact Page
    
    */
    Redux::setSection( $opt_name, array(
        'title' => __( 'Contact Section', 'fruitgarden' ),
        'id'    => 'contact-section',
        'desc'  => __( '', 'fruitgarden' ),
        'icon'  => 'el el-picture'
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Footer', 'fruitgarden' ),
        'id'         => 'fg-contact-page',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'contact-title',
                'type'     => 'text',
                'title'    => __( 'Contact Page Title', 'fruitgarden' ),
                'default'  => 'Contact Page',
            ),
            array( 
                'title'     => __( 'Form Title', 'fruitgarden' ),
                'subtitle'  => __( 'Enter your form title.', 'fruitgarden' ),
                'id'        => 'form-title',
                'default'   => 'Contact us!',
                'type'      => 'text',
            ),
            array( 
                'title'     => __( 'Contact Email', 'fruitgarden' ),
                'subtitle'  => __( 'Set your email address. This is where the contact form will send a message to.', 'fruitgarden' ),
                'id'        => 'contact-email',
                'default'   => 'yourname@yourdomain.com',
                'validate'  => 'email',
                'msg'       => 'Not a valid email address.',
                'type'      => 'text',
            ),
            array( 
                'title'     => __( 'Map title', 'fruitgarden' ),
                'subtitle'  => __( 'Enter you title above google map.', 'fruitgarden' ),
                'id'        => 'map-title',
                'default'   => 'We are here',
                'type'      => 'text',
            ),
            array( 
                'title'     => __( 'Latitude', 'fruitgarden' ),
                'subtitle'  => __( 'To set location you will need to find Latitude and Longitude numbers, you can find in <a href="http://www.latlong.net/" target="_blank">this site</a>.', 'fruitgarden' ),
                'id'        => 'latitude',
                'default'   => '43.007477',
                'validate'  => 'numeric',
                'msg'       => 'Not a valid latitude coordinates.',
                'type'      => 'text',
                'desc'      => 'Set your latitude coordinates.'
            ),
            array( 
                'title'     => __( 'Longitude', 'fruitgarden' ),
                'subtitle'  => __( 'To set location you will need to find Latitude and Longitude numbers, you can find in <a href="http://www.latlong.net/" target="_blank">this site</a>.', 'fruitgarden' ),
                'id'        => 'longitude',
                'default'   => '21.930845',
                'validate'  => 'numeric',
                'msg'       => 'Not a valid longitude coordinates.',
                'type'      => 'text',
                'desc'      => 'Set your longitude coordinates.'
            ),
            array( 
                'title'     => __( 'Addres title', 'fruitgarden' ),
                'subtitle'  => __( 'Enter your addres title', 'fruitgarden' ),
                'id'        => 'addres-title',
                'default'   => 'Addres',
                'type'      => 'text',
            ),
            array( 
                'title'     => __( 'Addres', 'fruitgarden' ),
                'subtitle'  => __( 'Enter your addres', 'fruitgarden' ),
                'id'        => 'fg-addres',
                'default'   => 'Addres',
                'type'      => 'text',
            ),
            array(
                'id'       => 'fg-phone',
                'type'     => 'text',
                'title'    => __( 'Phone', 'fruitgarden' ),
                'subtitle' => __( 'Enter your phone number.', 'fruitgarden' ),
                //'desc'     => __( 'This is the description field, again good for additional info.', 'fruitgarden' ),
                'validate' => 'numeric',
                'default'  => '0001234567',
            ),
        )
    ) );
    /*
    
        ---> Footer section
    
    */
    Redux::setSection( $opt_name, array(
        'title' => __( 'Footer Section', 'fruitgarden' ),
        'id'    => 'footer-section',
        'desc'  => __( '', 'fruitgarden' ),
        'icon'  => 'el el-picture'
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Footer', 'fruitgarden' ),
        'id'         => 'fg-footer-section',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'footer-copyright-text',
                'type'     => 'text',
                'title'    => __( 'Copyright', 'fruitgarden' ),
                'default'  => 'Copyright',
            ),
            array( 
                'id'        => 'social-icons',
                'title'     => __( 'Social Icons', 'fruitgarden' ),
                'subtitle'  => __( 'Arrange your social icons. Add complete URLs to your social profiles.', 'fruitgarden' ),
                'type'      => 'sortable',
                'label'     => true,
                'options'   => $social_options, 
            ),
        )
    ) );

    /*

        <--- END SECTIONS

    */

    /*
        This is a test function that will let you see when the compiler hook occurs.
        It only runs if a field set with compiler=>true is changed.
    */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
        }
    }

    // Custom function for the callback validation referenced above
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $return['error'] = $field;
                $field['msg']    = 'your custom error message';
            }

            if ( $warning == true ) {
                $return['warning'] = $field;
                $field['msg']      = 'your custom warning message';
            }

            return $return;
        }
    }

    // Custom function for the callback referenced above
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /*
        Custom function for filtering the sections array. Good for child themes to override or add to the sections.
        Simply include this function in the child themes functions.php file.
        NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
        so you must use get_template_directory_uri() if you want to use any of the built in icons
    */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => __( 'Section via hook', 'redux-framework-demo' ),
                'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /*
        Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
    */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }
    }

    // Filter hook for filtering the default value of any given field. Very useful in development mode.
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    // Removes the demo link and the notice of integrated demo from the redux-framework plugin
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }

