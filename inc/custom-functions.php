<?php
/*

	@package Fruit Garden

	--------------------
		CUSTOM FUNCTIONS
	--------------------

*/	
/*
	--------------------
		Remove Generator Version Number
	--------------------
*/
// Remove version string from js and css
function fruit_garden_remove_wp_version_strings( $src ){

	global $wp_version;
	parse_str( parse_url( $src, PHP_URL_QUERY ), $query );
	if ( !empty( $query['ver'] ) && $query['ver'] === $wp_version ) {
		$src = remove_query_arg( 'ver', $src );
	}
	return $src;
}
add_filter( 'script_loader_src', 'fruit_garden_remove_wp_version_strings' );
add_filter( 'style_loader_src', 'fruit_garden_remove_wp_version_strings' );

// Remove metatag generator from header
function fruit_garden_remove_meta_version(){
	return '';
}
add_filter( 'the_generator', 'fruit_garden_remove_meta_version' );

/*
	--------------------
		Admin Page
	--------------------
*/
function fruit_garden_admin_page(){

	//Generate Fruit Garden Admin Page
	add_menu_page( 'Fruit Garden Options', 'Garden', 'manage_options', 'fruit_garden', 'fruit_garden_theme_support', get_template_directory_uri() . '/img/admin-icon.png', 110 );

	//Generate Fruit Tree Nurserie Sub Pages
	//add_submenu_page( 'fruit_garden', 'Fruit Garden Theme Support Options', 'General', 'manage_options', 'fruit_garden', 'fruit_garden_theme_support' );

}
add_action( 'admin_menu', 'fruit_garden_admin_page' );

//Activate custom settings
add_action( 'admin_init', 'fruit_garden_custom_settings' );
function fruit_garden_custom_settings(){
	//Theme Support Options
	register_setting( 'fruit-garden-theme-support', 'site_logo' );
	register_setting( 'fruit-garden-theme-support', 'post_formats' );
	register_setting( 'fruit-garden-theme-support', 'custom_header' );
	register_setting( 'fruit-garden-theme-support', 'custom_background' );

	add_settings_section( 'fruit-garden-theme-options', '', 'fruit_garden_theme_options', 'fruit_garden_theme_support' );

	add_settings_field( 'site-logo', 'Site Logo', 'fruit_garden_site_logo', 'fruit_garden_theme_support', 'fruit-garden-theme-options' );
	add_settings_field( 'post-formats', 'Post Formats', 'fruit_garden_post_formats', 'fruit_garden_theme_support', 'fruit-garden-theme-options' );
	add_settings_field( 'custom-header', 'Custom Header', 'fruit_garden_custom_header', 'fruit_garden_theme_support', 'fruit-garden-theme-options' );
	add_settings_field( 'custom-background', 'Custom Background', 'fruit_garden_custom_background', 'fruit_garden_theme_support', 'fruit-garden-theme-options' );
}
// Subtitle in custom admin page
function fruit_garden_theme_options(){
	echo "Activate, deactivate and upload specific theme options";
}
// Site logo in custom admin page
function fruit_garden_site_logo(){
	$logo = esc_attr( get_option( 'site_logo' ) );
	if ( empty($logo) ) {
		echo '<input type="button" class="button button-secondary" value="Upload Site Logo" id="upload-button"><input type="hidden" id="site-logo" name="site_logo" value="" />';
	} else {
		echo '<input type="button" class="button button-secondary" value="Replace Site Logo" id="upload-button"><input type="hidden" id="site-logo" name="site_logo" value="'.$logo.'" /> <input type="button" class="button button-secondary" value="Remove" id="remove-logo">';
	}
}
// Activate and deactivate post formats
function fruit_garden_post_formats(){
	$options = esc_attr( get_option( 'post_formats' ) );
	$formats = array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' );
	$output = '';
	foreach ( $formats as $format ){
		$checked = ( @$options[$format] == 1 ? 'checked' : '' );
		$output .= '<label><input type="checkbox" id="'.$format.'" name="post_formats['.$format.']" value="1" '.$checked.' /> '.$format.'</label><br>';

	}
	echo $output;
}
// Activate and deactivate custom header
function fruit_garden_custom_header(){
	$options = esc_attr( get_option( 'custom_header' ) );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="custom_header" name="custom_header" value="1" '.$checked.' /> Activate the Custom Header</label>';
}
// Activate and deactivate custom background
function fruit_garden_custom_background(){
	$options = esc_attr( get_option( 'custom_background' ) );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="custom_background" name="custom_background" value="1" '.$checked.' /> Activate the Custom Background</label>';
}
// Menu functions
function fruit_garden_theme_support(){
	require_once( get_template_directory() . '/inc/templates/fruit-garden-theme-support.php' );
}
/*
	--------------------
		Theme support options
	--------------------
*/
$options = get_option( 'post_formats' );
$formats = array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' );
$output = array();
foreach ( $formats as $format ){
	$output[] = ( @$options[$format] == 1 ? $format : '' );
}
if( !empty( $options ) ){
	add_theme_support( 'post-formats', $output );
}

$header = get_option( 'custom_header' );
if( @$header ==1 ){
	add_theme_support( 'custom-header' );
}

$background = get_option( 'custom_background' );
if( @$background == 1 ){
	add_theme_support( 'custom-background' );
}
//Activate post thumbnails
add_theme_support( 'post-thumbnails' );

//Activate navigation menu
function fruit_garden_regiter_nav_menu(){
	register_nav_menu( 'primary_menu', 'Activate Navigation Menu' );
}
add_action( 'after_setup_theme', 'fruit_garden_regiter_nav_menu' );

/*
	--------------------
		Custom post types
	--------------------
*/
//Products Custom Post Type
function fruit_garden_custom_post_type() {
	//Products
	$products_labels = array(
		'name'				=> 'Products',
		'singular_name'		=> 'Product',
		'add_new'			=> 'Add Product',
		'all_items'			=> 'All Products',
		'add_new_item'		=> 'Add Product',
		'edit_item'			=> 'Edit Product',
		'new_item'			=> 'New Product',
		'view_item'			=> 'View Product',
		'search_item'		=> 'Search Products',
		'not_found'			=> 'No product found',
		'not_found-in_trash'=> 'No product found in trash',
		'parent_item_colon'	=> 'Parent Item'
	);
	$products_args = array(
		'labels'			=> $products_labels,
		'public'			=> true,
		'has_archive'		=> true,
		'publicly_queryable'=> true,
		'query_var'			=> true,
		'rewrite'			=> true,
		'capability_type'	=> 'post',
		'hierarchical'		=> false,
		'supports'			=> array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
		'menu_position'		=> 5,
		'menu_icon'			=> 'dashicons-feedback',
		'exclude_from_search'=> false
	);
	register_post_type( 'products', $products_args );
	//Tips
	$tips_labels = array(
		'name'				=> 'Tips',
		'singular_name'		=> 'Tip',
		'add_new'			=> 'Add Tip',
		'all_items'			=> 'All Tips',
		'add_new_item'		=> 'Add Tip',
		'edit_item'			=> 'Edit Tip',
		'new_item'			=> 'New Tip',
		'view_item'			=> 'View Tip',
		'search_item'		=> 'Search Tips',
		'not_found'			=> 'No tip found',
		'not_found-in_trash'=> 'No tip found in trash',
		'parent_item_colon'	=> 'Parent Item'
	);
	$tips_args = array(
		'labels'			=> $tips_labels,
		'public'			=> true,
		'has_archive'		=> true,
		'publicly_queryable'=> true,
		'query_var'			=> true,
		'rewrite'			=> true,
		'capability_type'	=> 'post',
		'hierarchical'		=> false,
		'supports'			=> array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
		'menu_position'		=> 6,
		'menu_icon'			=> 'dashicons-pressthis',
		'exclude_from_search'=> false
	);
	register_post_type( 'tips', $tips_args );
	//Gallery
	$galleries_labels = array(
		'name'				=> 'FG Gallery',
		'singular_name'		=> 'Gallery',
		'add_new'			=> 'Add Gallery',
		'all_items'			=> 'Gallery',
		'add_new_item'		=> 'Add Gallery',
		'edit_item'			=> 'Edit Gallery',
		'new_item'			=> 'New Gallery',
		'view_item'			=> 'View Gallery',
		'search_item'		=> 'Search Galleries',
		'not_found'			=> 'No tip found',
		'not_found-in_trash'=> 'No tip found in trash',
		'parent_item_colon'	=> 'Parent Item'
	);
	$galleries_args = array(
		'labels'			=> $galleries_labels,
		'public'			=> true,
		'has_archive'		=> true,
		'publicly_queryable'=> true,
		'query_var'			=> true,
		'rewrite'			=> true,
		'capability_type'	=> 'post',
		'hierarchical'		=> false,
		'supports'			=> array( 'title', 'thumbnail', 'revisions' ),
		'menu_position'		=> 7,
		'menu_icon'			=> 'dashicons-format-gallery',
		'exclude_from_search'=> false
	);
	register_post_type( 'fg-gallery', $galleries_args );
}
add_action( 'init', 'fruit_garden_custom_post_type' );

//Products Custom Taxonomies
function fruit_garden_custom_taxonomies() {
	//Products Taxonomies
	$products_labels = array(
		'name'				=> 'Product Types',
		'singular_name'		=> 'Product Type',
		'search_items'		=> 'Search Product Types',
		'all_items'			=> 'All Product Types',
		'parent_item'		=> 'Parent Product Types',
		'parent_item_colon'	=> 'Parent Product Types:',
		'edit_item'			=> 'Edit Product Type',
		'update_item'		=> 'Update Product Type',
		'add_new_item'		=> 'Add New Product Type',
		'new_item_name'		=> 'New Type Product Type',
		'menu_name'			=> 'Product Types'		
	);
	$products_args = array(
		'hierarchical'		=> true,
		'labels'			=> $products_labels,
		'show_ui'			=> true,
		'show_admin_column'	=> true,
		'query_var'			=> true,
		'rewrite'			=> array( 'slug' => 'product_type' )
	);
	register_taxonomy( 'product_type', array('products'), $products_args );
	//Tips Taxonomies
	$tips_labels = array(
		'name'				=> 'Tip Types',
		'singular_name'		=> 'Tip Type',
		'search_items'		=> 'Search Tip Types',
		'all_items'			=> 'All Tip Types',
		'parent_item'		=> 'Parent Tip Types',
		'parent_item_colon'	=> 'Parent Tip Types:',
		'edit_item'			=> 'Edit Tip Type',
		'update_item'		=> 'Update Tip Type',
		'add_new_item'		=> 'Add New Tip Type',
		'new_item_name'		=> 'New Type Tip Type',
		'menu_name'			=> 'Tip Types'		
	);
	$tips_args = array(
		'hierarchical'		=> true,
		'labels'			=> $tips_labels,
		'show_ui'			=> true,
		'show_admin_column'	=> true,
		'query_var'			=> true,
		'rewrite'			=> array( 'slug' => 'tip_type' )
	);
	register_taxonomy( 'tip_type', array('tips'), $tips_args );
}
add_action( 'init', 'fruit_garden_custom_taxonomies' );

/*
	--------------------
		Custom metabox
	--------------------
*/
//Define the metabox and field configurations
function fruit_garden_custom_metaboxes() {
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_fruitgarden';

   	//Initiate the metabox
    $cmb = new_cmb2_box( array(
        'id'            => 'fruitgarden_gallery_metabox',
        'title'         => __( 'Gallery', 'fruitgarden' ),
        'object_types'  => array( 'fg-gallery', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => false, // Show field names on the left
        'cmb_styles' 	=> true, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    	) 
    );
    //Create gallery
    $cmb->add_field( array(
	    'name' => 'Create Gallery',
	    'desc' => '',
	    'id'   => 'fruitgarden_gallery_field',
	    'type' => 'file_list',
	    'preview_size' => array( 130, 130 ), // Default: array( 50, 50 )
	    // Optional, override default text strings
	    'text' => array(
	        'add_upload_files_text' => 'Create Gallery', // default: "Add or Upload Files"
	        'remove_image_text' => '', // default: "Remove Image"
	        'file_text' => 'Replacement', // default: "File:"
	        'file_download_text' => 'Replacement', // default: "Download"
	        'remove_text' => 'Replacement', // default: "Remove"
	   		),
		) 	
    );
}
add_action( 'cmb2_admin_init', 'fruit_garden_custom_metaboxes' );

/*
	--------------------
		Enqueue widgests
	--------------------
*/
require_once get_template_directory() . '/inc/widgets/footer-products.php';
require_once get_template_directory() . '/inc/widgets/footer-tips.php';
require_once get_template_directory() . '/inc/widgets/footer-contact.php';
require_once get_template_directory() . '/inc/widgets/blog-products.php';
require_once get_template_directory() . '/inc/widgets/blog-tips.php';
require_once get_template_directory() . '/inc/widgets/sorte.php';

/*
	--------------------
		Register widgests
	--------------------
*/
function fruitgarden_register_widgets() {
    register_widget( 'Recent_Products' );
    register_widget( 'Recent_Tips' );
    register_widget( 'Footer_Contact_Form' );
    register_widget( 'Recent_Blog_Products' );
    register_widget( 'Recent_Blog_Tips' );
    register_widget( 'Sorte' );
}
add_action( 'widgets_init', 'fruitgarden_register_widgets' );

/*
	--------------------
		Register sidebars
	--------------------
*/
function fruit_garden_register_sidebar(){
	register_sidebar(array(
			'name'			=> __( 'Product Sidebar', 'fruitgarden' ),
			'id'			=> 'products-sidebar',
			'class'			=> 'products-sidebar',
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<h2 class="sidebar-title">',
			'after_title'   => '</h2>'
		)
	);
	register_sidebar(array(
			'name'			=> __( 'Tips Sidebar', 'fruitgarden' ),
			'id'			=> 'tips-sidebar',
			'class'			=> 'tips-sidebar',
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<h2 class="sidebar-title">',
			'after_title'   => '</h2>'			
		) 
	);
	register_sidebar(array(
			'name'			=> __( 'Footer Links', 'fruitgarden' ),
			'id'			=> 'footer-links',
			'class'			=> '',
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<h3 class="col-sm-3 col-xs-12">',
			'after_title'   => '</h3>'
		) 
	);
	register_sidebar(array(
			'name'			=> __( 'Footer Contact Info', 'fruitgarden' ),
			'id'			=> 'footer-contact-info',
			'class'			=> '',
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
		) 
	);
	register_sidebar(array(
			'name'			=> __( 'Home Page Sidebar', 'fruitgarden' ),
			'id'			=> 'home-page-sidebar',
			'class'			=> 'home-sidebar',
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<h3 class="home-sidebar-title">',
			'after_title'   => '</h3>'
		) 
	);
}
add_action( 'widgets_init', 'fruit_garden_register_sidebar' );

/*
	--------------------
		Excerpt length
	--------------------
*/
function fruit_garden_excerpt_length( $length ) {
	return 26;
}
add_filter( 'excerpt_length', 'fruit_garden_excerpt_length' );

/*
	--------------------
		Get Google map location and text for JavaScript file
	--------------------
*/
function get_location_lat() {
    global $fg_option;
    $location_lat = array( 'lat' => $fg_option['latitude'] );
    wp_localize_script( 'fruit-garden-front-scripts', 'map_location_lat', $location_lat );
}
add_action( 'wp_enqueue_scripts', 'get_location_lat' );

function get_location_lon() {
    global $fg_option;
    $location_lon = array( 'lon' => $fg_option['longitude'] );
    wp_localize_script( 'fruit-garden-front-scripts', 'map_location_lon', $location_lon );
}
add_action( 'wp_enqueue_scripts', 'get_location_lon' );

/*
	--------------------
		Output images from custom metabox in gallery cpt
	--------------------
*/
function fruit_garden_output_gallery( $fruitgarden_gallery_field, $img_size = 'full' ) {

    // Get the list of files
    $files = get_post_meta( get_the_ID(), $fruitgarden_gallery_field, 1 );

    // Loop through them and output an image
    foreach ( (array) $files as $attachment_id => $attachment_url ) { 
        ?>
		<a href="<?php echo esc_url( $attachment_url ); ?>">
			<img src="<?php echo esc_url( $attachment_url ); ?>" alt="" title="<?php echo apply_filters( 'title', esc_attr( get_the_title($attachment_id) ) ); ?>">
		</a>
        <?php
    }
}

/*
	--------------------
		Redux option
	--------------------
*/
if ( ! function_exists( 'fg_option' ) ) {
    function fg_option( $id, $fallback = false, $param = false ) {
        global $fg_option;
        if ( $fallback == false ) $fallback = '';
        $output = ( isset($fg_option[$id]) && $fg_option[$id] !== '' ) ? $fg_option[$id] : $fallback;
        if ( !empty($fg_option[$id]) && $param ) {
            $output = $fg_option[$id][$param];
        }
        return $output;
    }
}
