<?php
/*

	@package Fruit Garden

	--------------------
		ADMIN ENQUEUE FUNCTIONS
	--------------------

*/

function fruit_garden_load_admin_scripts( $hook ){

	if( 'toplevel_page_fruit_garden' != $hook ) { return; }

	wp_enqueue_media();

	wp_register_script( 'fruit-garden-admin-scripts', get_template_directory_uri() . '/js/fg-admin.js', array('jquery'), '1.0.0', true );
	wp_enqueue_script( 'fruit-garden-admin-scripts' );
	
}
add_action( 'admin_enqueue_scripts', 'fruit_garden_load_admin_scripts' );
/*

	--------------------
		FRONT-END ENQUEUE FUNCTIONS
	--------------------

*/

function fruit_garden_load_scripts(){
	//enqueue style
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.6', 'all' );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.6.3', 'all' );
	wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/inc/flexslider/flexslider.css', array(), '2.6.1', 'all' );
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.min.css', array(), '3.5.1', 'all' );
	wp_enqueue_style( 'simple-lightbox', get_template_directory_uri() . '/css/simplelightbox.min.css', array(), '', 'all' );
	wp_enqueue_style( 'fruit-garden', get_template_directory_uri() . '/css/fruit-garden.css', array(), '1.0.0', 'all' );
	
	//enqueue script
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.js', false, '1.12.4', true );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.6', true );
	wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/inc/flexslider/jquery.flexslider-min.js', array('jquery'), '2.3.0', true );
	wp_enqueue_script( 'validate', get_template_directory_uri() . '/js/jquery.validate.min.js', array('jquery'), '1.15.0', true );
	wp_enqueue_script( 'simple-lightbox', get_template_directory_uri() . '/js/simple-lightbox.min.js', array('jquery'), '1.15.0', true );
	if ( is_page_template( 'template-contact.php' ) ) {
		wp_enqueue_script( 'google-api', 'http://maps.googleapis.com/maps/api/js?key=AIzaSyCPUfvvlDkfCOtyEZKbI9zsmP4uXYwcySA&amp;', array('jquery'), '', true );
		wp_enqueue_script( 'google-map', get_template_directory_uri() . '/js/google-map.js', array('jquery'), '', true );

	}
	if ( is_single() ) {
		wp_enqueue_script( 'js-gototop', get_template_directory_uri() . '/js/jquery.gototop.min.js', array('jquery'), '0.1.0', true );
		wp_enqueue_script( 'content-singe', get_template_directory_uri() . '/js/content-single.js', array('jquery'), '', true );
	}
	wp_enqueue_script( 'fruit-garden-front-scripts', get_template_directory_uri() . '/js/fg-frontend.js', array('jquery'), '', true );
	
}
add_action( 'wp_enqueue_scripts', 'fruit_garden_load_scripts' );