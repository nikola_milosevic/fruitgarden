<h1>Theme Options</h1>
<?php settings_errors(); ?>

<form method="post" action="options.php" class="fruit-garden-general-form">
	<?php settings_fields( 'fruit-garden-theme-support' ); ?>
	<?php do_settings_sections( 'fruit_garden_theme_support' ); ?>
	<?php submit_button( 'Save Changes', 'primary', 'btnSubmit' ); ?>
</form>
