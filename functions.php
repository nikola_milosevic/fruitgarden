<?php

	/*
		Fruit Garden Functions file

		@package Fruit Garden
	*/

require_once get_template_directory() . '/inc/enqueue.php';
require_once get_template_directory() . '/inc/cmb2/init.php';
require_once get_template_directory() . '/redux/framework.php';
require_once get_template_directory() . '/inc/redux-config.php';
require_once get_template_directory() . '/inc/wp_bootstrap_navwalker.php';
require_once get_template_directory() . '/inc/custom-functions.php';