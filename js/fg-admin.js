jQuery(document).ready(function($){

	//Site logo
	var logoUploader;
	
	$( '#upload-button' ).on('click', function(e){

		e.preventDefault();
		if ( logoUploader ){
			logoUploader.open();
			return;
		}

		logoUploader = wp.media.frames.file_frame = wp.media({
			title: 'Choose a site logo',
			button: {
				text: 'Choose Logo'
			},
			multiple: true
		});

		logoUploader.on('select',function(){
			attachment = logoUploader.state().get('selection').first().toJSON();
			$('#site-logo').val(attachment.url);
		});

		logoUploader.open();

	});

	$( '#remove-logo' ).on('click', function(e){
		e.preventDefault();
		var answer = confirm("Are you sure you want to remove your Logo?");
		if (answer == true) {
			$('#site-logo').val('');
			$('.fruit-garden-general-form').submit();
		}
		return;
	});
    
});