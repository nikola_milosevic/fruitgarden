jQuery(document).ready(function($){
    $(function (){
        //Google map
        var lat = map_location_lat.lat;
        var lon = map_location_lon.lon;
        var myCenter = new google.maps.LatLng(lat,lon);

        var mapProp = {
            center: myCenter,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

        var marker = new google.maps.Marker({
            position: myCenter,
        });

        marker.setMap(map);
    });
});