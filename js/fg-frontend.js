jQuery(document).ready(function($){

    $(function(){
        var $gallery = $('.fg-gallery a').simpleLightbox();
    });
    
    // Slideshow
	$('.flexslider').flexslider({
        animation: "slide",
        prevText: "",
        nextText: ""
    });
    
    // Contact Form
 	$('#contact-form').on('submit', function() {

        var that = $(this),
            url = that.attr('action'),
            method = that.attr('method'),
            data = {};

        that.find('[name]').each(function(index, value) {

            var that = $(this),
                name = that.attr('name'),
                value = that.val();

            data[name] = value;
        });

        $.ajax({
            url: url,
            type: method,
            data: data,
            success: function(data){
                if(data==='fail'){
                    $("#error").fadeIn('slow').delay(2000).fadeOut('slow'); 
                }else{
                    $("#success").fadeIn('slow').delay(2000).fadeOut('slow');
                    that.find('[name]').val('');
                }
            }
        });
        return false;
    });

    $( "#contact-form" ).validate( {
        rules: {
            name: "required",
            subject: "required",
            email: {
                required: true,
                email: true
            },
            message: "required"
        },
        messages: {
            name: "Unesite vaše ime",
            subject: "Unesite naslov poruke",
            email: "Email adresa je obavezna",
            message: "Unesite poruku"
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            error.addClass( "help-block" );

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
        }
    });
});
