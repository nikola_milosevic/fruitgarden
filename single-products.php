<?php

	/*
		The single product file.

		This is the single product file, it is used to display a single product page.
	 
	 	@package Fruit Garden
	*/

get_header(); ?>

<div class="single-container">
	<div class="container">
		<div class="row">
			<?php if( have_posts() ):
				while( have_posts() ): the_post(); ?>

				<article id="post-<?php the_id(); ?>">
					<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 product-content">
						<h1 class="single-title"><span><?php the_title(); ?></span></h1> 
						<p><?php edit_post_link(); ?></p>
						<p><?php the_content(); ?></p>
					</div><!-- .product-content -->
					<div class="col-xs-8 col-sm-2 col-md-2 col-lg-2 products-sidebar">
						<?php $sidebarlogo = esc_attr( get_option('site_logo') ); ?>
						<div class="sidebar-logo" style="background-image: url(<?php print $sidebarlogo; ?>);">
						</div><!-- .sidebar-logo -->
						<?php get_sidebar( 'product-sidebar' ); ?>
					</div><!-- .product-sidebar -->
					<button class="gototop"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
				</article>

				<?php endwhile;
			endif; ?>
		</div><!-- .row -->
	</div><!-- .container-fluid -->
</div><!-- .single-container -->

<?php get_footer(); ?>